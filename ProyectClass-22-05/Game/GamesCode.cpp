#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_native_dialog.h>
#include <allegro5\allegro_primitives.h>
#include <allegro5\allegro_font.h>
#include <allegro5\allegro_ttf.h>
#include <allegro5\allegro_audio.h>
#include <allegro5\allegro_acodec.h>
#include "Objects.h"

//Globales
const int WIDTH = 800;
const int HEIGHT = 400;
const int NUM_BULLETS = 5;
const int NUM_ALIENS = 10;
enum KEYS { UP, DOWN, LEFT, RIGHT, SPACE };
bool keys[5] = { false,false,false,false,false };

ALLEGRO_SAMPLE *shoot = NULL;
ALLEGRO_SAMPLE *boom = NULL;
ALLEGRO_SAMPLE *song = NULL;
ALLEGRO_SAMPLE_INSTANCE *songInstance = NULL;

void InitShip(Ship &ship);
void DrawShip(Ship &ship);
void MoveShipUp(Ship &ship);
void MoveShipDown(Ship &ship);
void MoveShipLeft(Ship &ship);
void MoveShipRight(Ship &ship);

void InitBullet(Bullet bullet[], int size);
void DrawBullet(Bullet bullet[], int size);
void FireBullet(Bullet bullet[], int size, Ship &ship);
void UpdateBullet(Bullet bullet[], int size);
void CollideBullet(Bullet bullet[], int size, Alien aliens[], int aSize, ALLEGRO_BITMAP* coin);

void InitAlien(Alien aliens[], int size);
void DrawAlien(Alien aliens[], int size, ALLEGRO_BITMAP* image);
void StartAlien(Alien aliens[], int size);
void UpdateAlien(Alien aliens[], int size);
void CollideAlien(Alien aliens[], int aSize, Ship &ship);

int main(void)
{
	bool done = false;
	bool redraw = true;
	const int FPS = 60;
	bool isGameOver = false;
	bool play = false;

	//Jugador
	Ship ship;
	
	//Balas
	Bullet bullets[NUM_BULLETS];
	
	//Enemigos
	Alien aliens[NUM_ALIENS];

	ALLEGRO_DISPLAY *display = NULL;
	ALLEGRO_EVENT_QUEUE *event_queue = NULL;
	ALLEGRO_TIMER *timer = NULL;
	ALLEGRO_FONT *font18 = NULL;
	ALLEGRO_BITMAP *image = NULL;
	ALLEGRO_BITMAP *coin = NULL;

	if (!al_init())
		return -1;

	display = al_create_display(WIDTH, HEIGHT);

	if (!display)
		return -1;

	al_init_primitives_addon();
	al_install_keyboard();
	al_init_font_addon();
	al_init_ttf_addon();
	al_init_image_addon();
	al_install_audio();
	al_init_acodec_addon();

	event_queue = al_create_event_queue();
	timer = al_create_timer(1.0 / FPS);

	al_reserve_samples(10);

	shoot = al_load_sample("shot.ogg");
	boom = al_load_sample("boom.ogg");
	song = al_load_sample("song.ogg");

	songInstance = al_create_sample_instance(song);
	al_set_sample_instance_playmode(songInstance, ALLEGRO_PLAYMODE_LOOP);

	al_attach_sample_instance_to_mixer(songInstance, al_get_default_mixer());

	srand(time(NULL));


	InitShip(ship);
	InitBullet(bullets, NUM_BULLETS);
	InitAlien(aliens, NUM_ALIENS);

	font18 = al_load_font("arial.ttf", 18, 0);
	image = al_load_bitmap("AliensShip.png");
	coin = al_load_bitmap("Coin.png");

	al_register_event_source(event_queue, al_get_keyboard_event_source());
	al_register_event_source(event_queue, al_get_timer_event_source(timer));
	al_register_event_source(event_queue, al_get_display_event_source(display));

	al_start_timer(timer);
	while (!done)
	{
		ALLEGRO_EVENT ev;
		al_wait_for_event(event_queue, &ev);

		if (play)
		{
			if (ev.type == ALLEGRO_EVENT_TIMER)
			{
				redraw = true;
				if (keys[UP])
					MoveShipUp(ship);
				if (keys[DOWN])
					MoveShipDown(ship);
				if (keys[LEFT])
					MoveShipLeft(ship);
				if (keys[RIGHT])
					MoveShipRight(ship);

				if (!isGameOver)
				{
					UpdateBullet(bullets, NUM_BULLETS);
					StartAlien(aliens, NUM_ALIENS);
					UpdateAlien(aliens, NUM_ALIENS);
					CollideBullet(bullets, NUM_BULLETS, aliens, NUM_ALIENS, coin);
					CollideAlien(aliens, NUM_ALIENS, ship);

					if (ship.lives <= 0)
					{
						isGameOver = true;
						al_stop_sample_instance(songInstance);
						play = false;
					}
				}
			}
			else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
			{
				done = true;
			}

			if (ev.type == ALLEGRO_EVENT_KEY_DOWN)
			{
				switch (ev.keyboard.keycode)
				{
				case ALLEGRO_KEY_ESCAPE:
					done = true;
					break;
				case ALLEGRO_KEY_UP:
					keys[UP] = true;
					break;
				case ALLEGRO_KEY_DOWN:
					keys[DOWN] = true;
					break;
				case ALLEGRO_KEY_LEFT:
					keys[LEFT] = true;
					break;
				case ALLEGRO_KEY_RIGHT:
					keys[RIGHT] = true;
					break;
				case ALLEGRO_KEY_SPACE:
					keys[SPACE] = true;
					FireBullet(bullets, NUM_BULLETS, ship);
					break;
				default:
					break;
				}
			}
			else if (ev.type == ALLEGRO_EVENT_KEY_UP)
			{
				switch (ev.keyboard.keycode)
				{
				case ALLEGRO_KEY_ESCAPE:
					done = true;
					break;
				case ALLEGRO_KEY_UP:
					keys[UP] = false;
					break;
				case ALLEGRO_KEY_DOWN:
					keys[DOWN] = false;
					break;
				case ALLEGRO_KEY_LEFT:
					keys[LEFT] = false;
					break;
				case ALLEGRO_KEY_RIGHT:
					keys[RIGHT] = false;
					break;
				case ALLEGRO_KEY_SPACE:
					keys[SPACE] = false;
					break;
				default:
					break;
				}
			}

			if (redraw&& al_is_event_queue_empty(event_queue))
			{
				redraw = false;

				if (!isGameOver)
				{
					DrawShip(ship);
					DrawBullet(bullets, NUM_BULLETS);
					DrawAlien(aliens, NUM_ALIENS, image);
					al_draw_textf(font18, al_map_rgb(255, 0, 255), 5, 5, 0, "El Jugador tiene %i vidas.", ship.lives);
					//al_draw_bitmap(image, 30, 30, 0);
				}
				else
				{
					al_draw_textf(font18, al_map_rgb(0, 255, 255), WIDTH / 2, HEIGHT / 2, ALLEGRO_ALIGN_CENTER, "Game Over");
				}

				al_flip_display();
				al_clear_to_color(al_map_rgb(0, 0, 0));
			}
		}
		else
		{
			if (ev.type == ALLEGRO_EVENT_KEY_DOWN)
			{
				switch (ev.keyboard.keycode)
				{
				case ALLEGRO_KEY_ESCAPE:
					done = true;
					break;
				case ALLEGRO_KEY_ENTER:
					play = true;
					ship.lives = 3;
					break;
				default:
					break;
				}
			}

			if (redraw&& al_is_event_queue_empty(event_queue))
			{
				redraw = false;

				if (!isGameOver)
				{
					al_play_sample_instance(songInstance);
					al_draw_textf(font18, al_map_rgb(255, 255, 255), WIDTH / 2, HEIGHT / 3, ALLEGRO_ALIGN_CENTER, "Enter para comenzar a jugar.");
					al_draw_textf(font18, al_map_rgb(255, 255, 255), WIDTH / 2, HEIGHT / 2, ALLEGRO_ALIGN_CENTER, "Espacio para salir.");
				}

				al_flip_display();
				al_clear_to_color(al_map_rgb(0, 0, 0));
			}
		}
	}

	al_destroy_sample(shoot);
	al_destroy_sample(boom);
	al_destroy_sample(song);
	al_destroy_sample_instance(songInstance);
	al_destroy_display(display);
	al_destroy_bitmap(image);
	al_destroy_bitmap(coin);

	return 0;
}

void InitShip(Ship &ship)
{
	ship.x = 20;
	ship.y = HEIGHT / 2;
	ship.ID = PLAYER;
	ship.lives = 3;
	ship.speed = 7;
	ship.boundx = 6;
	ship.boundy = 7;
	ship.score = 0;
}
void DrawShip(Ship &ship)
{
	al_draw_filled_rectangle(ship.x, ship.y - 9, ship.x + 10, ship.y - 7, al_map_rgb(255, 0, 0));
	al_draw_filled_rectangle(ship.x, ship.y + 9, ship.x + 10, ship.y + 7, al_map_rgb(255, 0, 0));

	al_draw_filled_triangle(ship.x - 12, ship.y - 17, ship.x + 12, ship.y, ship.x - 12, ship.y + 17, al_map_rgb(0, 255, 0));
	al_draw_filled_rectangle(ship.x - 12, ship.y - 2, ship.x + 15, ship.y + 2, al_map_rgb(0, 0, 255));
}
void MoveShipUp(Ship &ship) 
{
	ship.y -= ship.speed;
	if (ship.y < 0)
		ship.y = 0;
}
void MoveShipDown(Ship &ship)
{
	ship.y += ship.speed;
	if (ship.y > HEIGHT)
		ship.y = HEIGHT;
}
void MoveShipLeft(Ship &ship)
{
	ship.x -= ship.speed;
	if (ship.x < 0)
		ship.x = 0;
}
void MoveShipRight(Ship &ship)
{
	ship.x += ship.speed;
	if (ship.x > 300)
		ship.x = 300;
}

void InitBullet(Bullet bullet[], int size)
{
	for (int i = 0; i < size; i++)
	{
		bullet[i].ID = BULLET;
		bullet[i].speed = 10;
		bullet[i].live = false;
	}
}
void DrawBullet(Bullet bullet[], int size)
{
	for (int i = 0; i < size; i++)
	{
		if (bullet[i].live)
			al_draw_filled_circle(bullet[i].x, bullet[i].y, 2, al_map_rgb(255, 255, 255));
	}
}
void FireBullet(Bullet bullet[], int size, Ship &ship)
{
	for (int i = 0; i < size; i++)
	{
		if (!bullet[i].live)
		{
			bullet[i].x = ship.x + 17;
			bullet[i].y = ship.y;
			bullet[i].live = true;

			al_play_sample(shoot, 1, 0, 1, ALLEGRO_PLAYMODE_ONCE, 0);
			break;
		}
	}
}
void UpdateBullet(Bullet bullet[], int size)
{
	for (int i = 0; i < size; i++)
	{
		if (bullet[i].live)
		{
			bullet[i].x += bullet[i].speed;
			if (bullet[i].x > WIDTH)
				bullet[i].live = false;
		}
	}
}
void CollideBullet(Bullet bullet[], int size, Alien aliens[], int aSize, ALLEGRO_BITMAP* coin)
{
	for (int i = 0; i < size; i++)
	{
		if (bullet[i].live)
		{
			for (int j = 0; j < aSize; j++)
			{
				if (aliens[j].live)
				{
					if (bullet[i].x > (aliens[j].x - aliens[j].boundx) &&
						bullet[i].x< (aliens[j].x + aliens[j].boundx) &&
						bullet[i].y>(aliens[j].y - aliens[j].boundy) &&
						bullet[i].y < (aliens[j].y + aliens[j].boundy))
					{
						bullet[i].live = false;
						aliens[j].live = false;
						al_play_sample(boom, 1, 0, 1, ALLEGRO_PLAYMODE_ONCE, 0);
						al_draw_bitmap(coin, aliens[j].x, aliens[j].y, 0);
					}
				}
			}

		}
	}
}

void InitAlien(Alien aliens[], int size)
{
	for (int i = 0; i < size; i++)
	{
		aliens[i].ID = ENEMY;
		aliens[i].live = false;
		aliens[i].speed = 5;
		aliens[i].boundx = 28;
		aliens[i].boundy = 28;
	}
}
void DrawAlien(Alien aliens[], int size, ALLEGRO_BITMAP* image)
{
	for (int i = 0; i < size; i++)
	{
		if (aliens[i].live)
		{
			//al_draw_filled_circle(aliens[i].x, aliens[i].y, 20, al_map_rgb(255, 0, 0));
			al_draw_bitmap(image, aliens[i].x, aliens[i].y, 0);
		}
	}
}
void StartAlien(Alien aliens[], int size) 
{
	for (int i = 0; i < size; i++)
	{
		if (!aliens[i].live)
		{
			if (rand() % 500 == 0)
			{
				aliens[i].live = true;
				aliens[i].x = WIDTH;
				aliens[i].y = 30 + rand() % (HEIGHT - 60);

				break;
			}
		}
	}
}
void UpdateAlien(Alien aliens[], int size)
{
	for (int i = 0; i < size; i++)
	{
		if (aliens[i].live)
		{
			aliens[i].x -= aliens[i].speed;
		}
	}
}
void CollideAlien(Alien aliens[], int aSize, Ship &ship)
{
	for (int i = 0; i < aSize; i++)
	{
		if (aliens[i].live)
		{
			if (aliens[i].x - aliens[i].boundx < ship.x + ship.boundx &&
				aliens[i].x + aliens[i].boundx > ship.x + ship.boundx &&
				aliens[i].y - aliens[i].boundy < ship.y + ship.boundy &&
				aliens[i].y + aliens[i].boundy > ship.y - ship.boundy)
			{
				ship.lives--;
				aliens[i].live = false;
			}
			else if (aliens[i].x < 0)
			{
				aliens[i].live = false;
				ship.lives--;
			}
		}
	}
}